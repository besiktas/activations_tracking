from cuml.manifold import UMAP as umap_cuml
import numpy as np
from sklearn.base import BaseEstimator
from sklearn import preprocessing
from sklearn import linear_model, metrics, model_selection, preprocessing, svm
import torch.multiprocessing as mp
import umap as umap_orig

import asyncio
import socketserver
import functools
from typing import Any, Dict, List

import config


def get_classification_report(clf, x, y):
    # OLD FUNCTION TO REFACTOR
    x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size=0.3, random_state=config.RANDOM_STATE)
    clf.fit(x_train, y_train)
    y_preds = clf.predict(x_test)

    acc_score = metrics.accuracy_score(y_test, y_preds)
    acc_per_class = metrics.confusion_matrix(y_test, y_preds)
    acc_per_class = acc_per_class.diagonal() / acc_per_class.sum(axis=1)

    report = metrics.classification_report(y_test, y_preds)
    return report


def make_umap():
    umap_obj = umap_cuml(n_components=10, random_state=42, min_dist=0.0, output_type="numpy", spread=5.0)
    return umap_obj


def calculate_umap_orig(activation, umap_obj=None):
    activation = np.reshape(activation, (len(activation), -1))

    if umap_obj is None:
        umap_obj = umap_orig.UMAP(n_components=5, random_state=config.RANDOM_STATE, min_dist=0.0, spread=5.0)
        umap_obj.fit(activation)

    embedded_activations = umap_obj.transform(activation)
    return umap_obj, embedded_activations


def calculate_umap_cuml(activation, umap_obj=None):
    umap_config = {"n_components": 10, "min_dist": 0.0, "spread": 5.0}

    if isinstance(activation, list):
        activation = np.array(activation)

    activation = np.reshape(activation, (len(activation), -1))

    if umap_obj is None:
        umap_obj = umap_cuml(random_state=config.RANDOM_STATE, output_type="numpy", **umap_config)
        # if i just use fit for some reason i get AttributeError with cuml
        embedded_activations = umap_obj.fit_transform(activation)

    embedded_activations = umap_obj.transform(activation)
    return umap_obj, embedded_activations


class ActivationsModel:
    """dataclass to hold linear models i am comparing on activations"""

    preprocess_data: bool = False
    use_scaled: bool = False
    n_jobs: int = 4

    def __init__(self, clf: BaseEstimator, clf_params: Dict[str, Any], x_name: str = "normal") -> None:
        self.clf = clf(**clf_params, random_state=config.RANDOM_STATE)
        self.clf_params = clf_params
        self._x = None
        self._y = None

        self.x_name = x_name

        # default results
        self.fit = False
        self.acc = 0.0
        self.scale_x = False

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    def __lt__(self, other):
        return self.acc < other.acc

    def print(self):
        print(f"{config.BIG_LINE_SEP}")
        print(f"-->on {self.x_name} with {self.clf}| x.shape: {self.x.shape} acc: {self.acc:0.3f}")
        # print(f"    -->params: {self.clf_params}")
        print(f"{config.BIG_LINE_SEP}")

    def preprocess_data_func(self) -> np.array:
        self.scaler = preprocessing.StandardScaler()
        return self.scaler.fit_transform(np.reshape(self.x, (len(self.x), -1)))

    def get_results(self, use_cross_val: bool = False) -> None:
        if use_cross_val:
            self._use_cross_val()
            return

        # just using simple accuracy
        self.acc = self.accuracy_score(self.x)

        if (tmp_acc := (self.accuracy_score(self.preprocess_data_func()))) > self.acc:
            self.scale_x = True
            self.acc = tmp_acc

    def accuracy_score(self, x: np.array) -> float:
        x_train, x_test, y_train, y_test = model_selection.train_test_split(
            x, self.y, test_size=0.3, random_state=config.RANDOM_STATE
        )
        if self.fit is False:
            self.clf.fit(np.reshape(x_train, (len(x_train), -1)), y_train)

        return self.clf.score(np.reshape(x_test, (len(x_test), -1)), y_test)

    def _use_cross_val(self):
        # if i want to use cross val later:
        self.acc = np.mean(self.cross_val(self.x))

        if (tmp_acc := np.mean(self.cross_val(self.preprocess_data_func()))) > self.acc:
            self.acc = tmp_acc
            self.scale_x = True

    def cross_val(self, x) -> float:
        acc = model_selection.cross_val_score(
            self.clf, np.reshape(x, (len(x), -1)), self.y, cv=4, n_jobs=self.n_jobs, scoring="accuracy"
        )
        return acc


class ActivationsQueue:
    """goal for this was to make activations stuff concurrent/async but idk if its possible"""

    def __init__(self, models: List[ActivationsModel] = []) -> None:
        self.umap_objs = {}
        self.lm_scores = {}
        self.models = models

        self.task_queue = mp.Queue()
        self.done_queue = mp.Queue()

    def make_embedded(self, key, arr, keep_func=False):

        umap_obj = self.umap_objs.get(key, None) if keep_func else None

        umap_obj, layer_embedded_activations = calculate_umap_cuml(arr, umap_obj=umap_obj)

        if keep_func:
            self.umap_objs[key] = umap_obj

        return layer_embedded_activations

    def make_lm(self, x, y, key):
        # just moved this stuff in here for now but probably should make it more modular

        for model in self.models:
            model.x, model.y = x, y
            print(f"starting... activations models for layer: {key}")
            model.get_results()

        self.models.sort(key=lambda x: x.acc)

        return self.models

    def save_lm_score(self, key, score):
        if key not in self.lm_scores:
            self.lm_scores[key] = []

        self.lm_scores[key].append(score)

    async def run(self, host: str = "localhost", port: int = 8989):
        """idea would be to be pass stuff to compute after training step done and then next training step starts while this is processing

        Raises:
            NotImplementedError: [description]
        """

        def handle_client():
            return

        server = await asyncio.start_server(handle_client, host, port)

        async with server:
            await server.serve_forever()


if __name__ == "__main__":
    queue = ActivationsQueue()
    asyncio.run(queue.run())
