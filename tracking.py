import matplotlib.pyplot as plt
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim


import asyncio
import copy
import time
from typing import Any, Callable, Dict, List, Tuple

from sklearn import linear_model, metrics, model_selection, svm

import config

from activations_compare import ActivationsQueue, calculate_umap_cuml, make_umap

device = torch.device(config.CUDA_DEVICE if torch.cuda.is_available() else "cpu")


class CaptureOutput:
    def __init__(self, module):
        self.output = None
        self.handle = module.register_forward_hook(self)

    def __call__(self, module, input, output):
        self.output = output

    def __del__(self):
        self.handle.remove()


class CaptureManager:
    def __init__(self, module: torch.nn.Module, attach_subset: int = None, activations_queue: ActivationsQueue = None):
        self.module = module
        self.attach_subset = attach_subset
        self.capture = {}
        self.outputs = {}
        self.labels = []

        self.activations_queue = activations_queue
        self._mp = False

    def attach(self):
        # allow taking a subset so we dont have 10x10 and get confused for some tensors and instead have [subset x num_classes]

        layers_to_attach = (
            list(self.module.named_children())[0 : self.attach_subset]
            if self.attach_subset
            else list(self.module.named_children())
        )

        for key, layer in layers_to_attach:
            self.capture[key] = CaptureOutput(layer)

            # if key not in self.lm_scores:
            #     self.lm_scores[key] = []

    def detach(self):
        for key in self.capture.keys():
            self.capture[key] = None

        self.clear()

    def clear(self):
        """clears the values in output/labels"""
        for key in self.outputs.keys():
            self.outputs[key] = []

        self.labels = []

    def batch_update(self, labels: np.array = None):
        """
        collect all the captures for the batch
        """
        if labels is not None:
            self.labels.extend(labels)

        for key in self.capture.keys():
            if key not in self.outputs:
                self.outputs[key] = []

            for output in self.capture[key].output.detach().cpu().numpy():
                self.outputs[key].append(output)

    def update(self, epoch: int = 0):
        """
        update is my catch all func for stuff related to activations
        """
        # for

        fig, axs = plt.subplots(len(self.outputs), 2, figsize=(10, 4 * len(self.outputs)))

        cmap = plt.get_cmap("tab10")

        for idx, key in enumerate(self.outputs.keys()):
            self.outputs[key] = np.array(self.outputs[key])
            plot_helper = lambda col, acts: axs[idx, col].scatter(acts[:, 0], acts[:, 1], c=self.labels, cmap=cmap)

            # OR
            # activations = await self.activations_queue.get(key)

            # OLD BELOW
            activations = self.activations_queue.make_embedded(key, self.outputs[key], keep_func=True)
            plot_helper(0, activations)
            # self.plot_helper(axs, idx, 0, activations)

            activations = self.activations_queue.make_embedded(key, self.outputs[key], keep_func=False)
            plot_helper(0, activations)
            # self.plot_helper(axs, idx, 1, activations)

            models = self.activations_queue.make_lm(x=activations, y=self.labels, key=key)

            acc_score = models[-1].acc

            axs[idx, 0].set_title(f"{key} - initial - accuracy: {acc_score}")
            axs[idx, 1].set_title(f"{key} - new - accuracy: {acc_score}")

            # save best lm score
            print(f"got acc score for key : {acc_score} {key}")
            self.activations_queue.save_lm_score(key, acc_score)

        plt.tight_layout()
        fig.savefig(f"images/{epoch}_umap_cuml.png")
        plt.close(fig)

    def plot_helper(self, axs: plt.Axes, row_idx: int, col_idx: int, activations: np.array):
        axs[row_idx, col_idx].scatter(
            activations[:, 0],
            activations[:, 1],
            c=self.labels,
            cmap=plt.get_cmap("tab10"),
        )

    def compare_model(self):
        pass


class TrainerHelper:
    def __init__(self, model: nn.Module, dataloaders: Dict[str, torch.utils.data.DataLoader]) -> None:
        super().__init__()
        self.model = model
        self.model.share_memory()

        self.model = self.model.to(device)

        self.dataloaders = dataloaders
        self.dataset_sizes = {phase: len(dataloader_.dataset) for phase, dataloader_ in dataloaders.items()}

        #
        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.model.parameters(), lr=0.0001)
        self.scheduler = optim.lr_scheduler.StepLR(self.optimizer, step_size=7, gamma=0.1)

        self.best_model_wts = copy.deepcopy(self.model.state_dict())
        self.best_acc = 0.0

        self.capture_manager = None
        self.stop_early = False

        self.callbacks: List[Callback] = []
        self.clear_running_vals()

    def clear_running_vals(self):
        self.running_loss = 0.0
        self.running_corrects = 0

    def batch_update_loss_and_corrects(
        self, loss: torch.Tensor, inputs: torch.Tensor, preds: torch.Tensor, labels: torch.Tensor
    ) -> None:
        self.running_loss += loss.item() * inputs.size(0)
        self.running_corrects += torch.sum(preds == labels.data)

    def post_step(self, phase: str) -> None:
        self.epoch_loss = self.running_loss / self.dataset_sizes[phase]
        self.epoch_acc = self.running_corrects.double() / self.dataset_sizes[phase]
        print(f"{phase.upper().ljust(10)}| Loss: {self.epoch_loss:.4f} Acc: {self.epoch_acc:.4f}")

    def train_step(self) -> None:
        step = "train"

        self.model.train()
        self.clear_running_vals()
        for inputs, labels in self.dataloaders[step]:
            inputs = inputs.to(device)
            labels = labels.to(device)

            self.optimizer.zero_grad()

            with torch.set_grad_enabled(True):

                outputs = self.model(inputs)

                _, preds = torch.max(outputs, 1)
                loss = self.criterion(outputs, labels)

                loss.backward()
                self.optimizer.step()

            self.batch_update_loss_and_corrects(loss, inputs, preds, labels)

        self.scheduler.step()
        self.post_step(step)

    # would be cool to use a @decorator on the batches if i broke that off
    def val_step(self) -> None:
        step = "val"
        self.clear_running_vals()
        self.model.eval()

        # batches of val data
        for inputs, labels in self.dataloaders[step]:
            inputs = inputs.to(device)
            labels = labels.to(device)

            self.optimizer.zero_grad()

            with torch.set_grad_enabled(False):
                outputs = self.model(inputs)
                _, preds = torch.max(outputs, 1)
                loss = self.criterion(outputs, labels)

            self.batch_update_loss_and_corrects(loss, inputs, preds, labels)

            self.capture_manager.batch_update(labels=labels.detach().cpu().numpy())

        # after you are done with all the data for val
        if self.epoch_acc > self.best_acc:
            self.best_acc = self.epoch_acc
            self.best_model_wts = copy.deepcopy(self.model.state_dict())

        self.post_step(step)

    def train_init(self) -> None:
        if self.capture_manager:
            self.capture_manager.init

    def train(self, num_epochs: int) -> None:
        for epoch in range(num_epochs):
            print(f"{'EPOCH'.ljust(7)}==>{epoch}")

            # train related
            self.capture_manager.detach()
            self.train_step()

            # val related
            self.capture_manager.attach()
            self.val_step()

            # await
            # asyncio.run(self.capture_manager.update(epoch=epoch))
            # asyncio.run(self.capture_manager.update(epoch=epoch))
            self.capture_manager.update(epoch=epoch)

            for callback in self.callbacks:
                callback()

            # allow early exit
            if self.stop_early:
                break

    def attach_callback(self, callback: "Callback") -> None:
        callback.trainer = self
        # callback.set_trainer(self)
        callback.capture_manager = self.capture_manager

        self.callbacks.append(callback)

    def attach_capture_manager(self, capture_manager: CaptureManager) -> None:
        self.capture_manager = capture_manager

    def post_val_step(self, *args, **kwargs):
        pass
