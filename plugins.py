import torch

from typing import List

from tracking import TrainerHelper, CaptureManager
import config


class Callback:
    trainer_ref: TrainerHelper = None
    capture_ref: CaptureManager = None

    # def __init__(self):
    #     pass
    #     self.trainer_ref = None
    #     self.capture_ref = None

    @property
    def trainer(self):
        return self.trainer_ref

    @trainer.setter
    def trainer(self, trainer_ref: TrainerHelper):
        self.trainer_ref = trainer_ref

    @property
    def capture_manager(self):
        return self.capture_ref

    @capture_manager.setter
    def capture_manager(self, capture_ref: CaptureManager):
        self.capture_ref = capture_ref


class EarlyStopping(Callback):
    def __init__(self, delta: float = 0.0001, patience: int = 3):
        super().__init__()
        self.delta = delta
        self.counter = 0
        self.patience = patience

        self.best_score = None

    def __call__(self, score=None) -> bool:
        if score is None:
            score = self.trainer_ref.epoch_acc

        if self.best_score is None:
            self.best_score = score

        elif score < self.best_score + self.delta:
            self.counter += 1
            if self.counter >= self.patience:
                self.trainer_ref.stop_early = True
        else:
            self.best_score = score
            self.counter = 0


class ActivationsBasecase(Callback):
    # We plot the activation axis in terms of standard deviations of activation from zero, since activations have an arbitrary scale.
    def __init__(self, input_shape: List[int], type=None):
        super().__init__()
        self.x = torch.zeros(input_shape)
        self.activations = None

    def __call__(self):
        self.capture_manager.clear()
        # breakpoint()
        self.output = self.trainer_ref.model(self.x.to(config.CUDA_DEVICE))
        self.activations = self.capture_ref.capture
