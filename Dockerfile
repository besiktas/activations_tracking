FROM rapidsai/rapidsai-dev:0.19-cuda11.2-devel-ubuntu18.04-py3.8
# RUN pip3 install torch torchvision torchaudio
RUN /opt/conda/envs/rapids/bin/pip install torch torchvision torchaudio mlflow neptune-client psutil lera pythreejs pytorch-ignite pytorch-forecasting cleverhans tensorly

# not sure why some install but not others so just installing above here as well
RUN /opt/conda/envs/rapids/bin/python -m pip install torch torchvision torchaudio mlflow neptune-client psutil lera pythreejs pytorch-ignite pytorch-forecasting cleverhans tensorly

RUN mkdir -p /activations_tracking/
WORKDIR /activations_tracking/

