
# docker commands
to build
`docker build -t graham/activations .`

<!-- docker run --gpus all --rm -it --network=host -v /data/graham:/data -v $(pwd):/activations_tracking -p 8888:8888 -p 8787:8787 -p 8786:8786 graham/activations:latest -->
`docker run --gpus all --rm -it  -v /data/graham:/data -v $(pwd):/activations_tracking -v $(pwd):/rapids/notebooks/host/activations_tracking -p 8888:8888 -p 8787:8787 -p 8786:8786 -p 8889:8889 graham/activations:latest`

using this base image

https://github.com/rapidsai/docker

# jupyer commands

nohup jupyter-lab --allow-root --ip=0.0.0.0 --no-browser --NotebookApp.token='' > /dev/null 2>&1 &

# mlflow
`mlflow server --default-artifact-root /data/graham/mlflow/artifacts  --backend-store-uri sqlite:////data/graham/mlflow/sql/mlfow.db --port 8889 --host 0.0.0.0`