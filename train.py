import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
from torchvision import datasets, models, transforms

from sklearn import linear_model
from sklearn.model_selection import train_test_split

import copy
import os
import time
from typing import Dict, List

import config
import tracking
import activations_compare

from plugins import ActivationsBasecase, EarlyStopping

np.seterr(divide="ignore")


def plot_lm_scores(lm_scores):
    fig, axs = plt.subplots(len(lm_scores), 1, figsize=(10, 4 * len(lm_scores)))
    for idx, key in enumerate(lm_scores.keys()):
        scores = np.array(lm_scores[key])

        for j in range(scores.shape[-1]):
            axs[idx].plot(
                scores[:, j],
            )
        axs[idx].set_title(f"key={key}")

    # plt.tight_layout()
    fig.savefig(f"images/lm_scores.png")
    plt.close(fig)


def plot_lm_grad(lm_scores: Dict[str, List]):
    get_grad = lambda x: [value[x] for value in lm_scores.values()]
    fig, axs = plt.subplots(1, 1, figsize=(10, 10), sharex=True)
    num_epochs = len(list(lm_scores.values()))
    # grads = {val: get_grad(val) for val in range(0, num_epochs, 5)}
    # axs.plot()


# DATA RELATED


def subset_dataset(dataset: datasets.ImageFolder, n: int):
    idxs, _ = train_test_split(range(len(dataset)), train_size=n, stratify=dataset.targets)
    return torch.utils.data.Subset(dataset, idxs)


# === === === === === ===
# --> DATA SET UP
# === === === === === ===

data_transforms = {
    "train": transforms.Compose(
        [
            transforms.RandomResizedCrop(config.IMG_SIZE),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        ]
    ),
    "val": transforms.Compose(
        [
            transforms.Resize(config.IMG_SIZE),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        ]
    ),
}


image_datasets = {x: datasets.ImageFolder(config.ORIGINAL_DATA_PATH / x, data_transforms[x]) for x in ["train", "val"]}
class_names = image_datasets["train"].classes

if config.USE_TRAIN_SUBSET:
    image_datasets["train"] = subset_dataset(image_datasets["train"], config.USE_TRAIN_SUBSET)
if config.USE_VAL_SUBSET:
    image_datasets["val"] = subset_dataset(image_datasets["val"], config.USE_VAL_SUBSET)

dataloaders = {
    "train": torch.utils.data.DataLoader(image_datasets["train"], batch_size=4, shuffle=True, num_workers=4),
    "val": torch.utils.data.DataLoader(image_datasets["val"], batch_size=4, shuffle=False, num_workers=4),
}

dataset_sizes = {x: len(image_datasets[x]) for x in ["train", "val"]}

activations_labels = {}
activations = {}

# === === === === === ===
# --> MODEL SET UP
# === === === === === ===

model = models.resnet18()

# make last layer work with our dataset
last_layer = list(model.children())[-1]
last_layer = nn.Linear(last_layer.in_features, len(class_names))
model.share_memory()


trainer = tracking.TrainerHelper(model, dataloaders)
earlystopping = EarlyStopping()


models_to_test = [
    activations_compare.ActivationsModel(
        clf=linear_model.SGDClassifier,
        clf_params={
            "loss": "hinge",
            "max_iter": 100,
            "early_stopping": True,
        },
        x_name="embedded",
    ),
    activations_compare.ActivationsModel(
        clf=linear_model.SGDClassifier,
        clf_params={
            "loss": "log",
            "max_iter": 100,
            "early_stopping": True,
        },
        x_name="embedded",
    ),
]

activations_queue = activations_compare.ActivationsQueue(models=models_to_test)

capture_manager = tracking.CaptureManager(model, activations_queue=activations_queue)
trainer.attach_capture_manager(capture_manager)

activations_basecase = ActivationsBasecase(input_shape=(3, *config.IMG_SHAPE))
trainer.attach_callback(activations_basecase)

num_epochs = 2


trainer.train(num_epochs=num_epochs)

# tracking.plot_lm_scores(capture_manager.lm_scores)
# lm_scores = np.array([arr for arr in capture_manager.lm_scores.values()])
# tracking.plot_lm_grad(capture_manager.lm_scores)
