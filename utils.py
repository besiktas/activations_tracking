# import mlflow
import functools
import time


def timer(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        out = func(*args, **kwargs)
        end_time = time.perf_counter()
        print(f"ELAPSED TIME:{end_time - start_time:0.4f}")
        return out

    return wrapper
