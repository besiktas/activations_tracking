from pathlib import Path

RANDOM_STATE = 42

CUDA_DEVICE = "cuda:1"

BIGGER = 10
USE_TRAIN_SUBSET = 800 * BIGGER
USE_VAL_SUBSET = 100 * BIGGER

VERBOSITY = 1
PARALLEL_JOBS = 4

# PATHS
MODEL_PATH = Path("/data/centroidtracking/models/latest")
SAVED_DATA_PATH = Path("/data/centroidtracking/data/latest")

ORIGINAL_DATA_PATH = Path("/data/data/fastai/imagenette2-320")
# SAVED_DATA

# use this value unless i refactor how im generating activations to not be 1 np array
OOM_SAMPLE_SIZE = 200

# image/model related
BATCH_SIZE = 32
IMG_SIZE = (320, 320)
IMG_SHAPE = (3,) + IMG_SIZE

BIG_LINE_SEP = f"{' '.join(['===']*3)}"

labels = {
    "n01440764": "tench",
    "n02102040": "English springer",
    "n02979186": "cassette player",
    "n03000684": "chain saw",
    "n03028079": "church",
    "n03394916": "French horn",
    "n03417042": "garbage truck",
    "n03425413": "gas pump",
    "n03445777": "golf ball",
    "n03888257": "parachute",
}

labels_id_array = list(labels.keys())
labels_array = list(labels.values())
